class UserEntities {
  final String createdAt;
  final String name;
  final String email;
  final String city;
  final String avatar;
  final String id;
  final String birthdate;

  const UserEntities({
    required this.name,
    required this.avatar,
    required this.email,
    required this.id,
    required this.city,
    required this.createdAt,
    required this.birthdate
  });

}