import 'package:estudo_objectbox/domain/entities/user_entities.dart';

abstract class UserRepository {
  Future<List<UserEntities>> getAllUsers();
}