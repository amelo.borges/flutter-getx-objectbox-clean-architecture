import 'package:estudo_objectbox/presentation/view/user/user_binding.dart';
import 'package:estudo_objectbox/routes/app_routes.dart';
import 'package:estudo_objectbox/presentation/view/user/user_view.dart';
import 'package:estudo_objectbox/presentation/view/users/users_binding.dart';
import 'package:estudo_objectbox/presentation/view/users/users_view.dart';
import 'package:get/get.dart';

abstract class AppPage {
  static List<GetPage> items = [
    GetPage(
      transition: Transition.cupertino,
      name: AppRoutes.users,
      page: () => const UsersView(),
      binding: UsersBinding(),
    ),
    GetPage(
      transition: Transition.cupertino,
      name: AppRoutes.user,
      page: () => const UserView(),
      binding: UserBinding(),
    ),
  ];
}
