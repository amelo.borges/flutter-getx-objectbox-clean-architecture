class UserModel {
  String createdAt;
  String name;
  String email;
  String city;
  String avatar;
  String id;
  String birthdate;

  UserModel({
    required this.name,
    required this.avatar,
    required this.email,
    required this.id,
    required this.city,
    required this.createdAt,
    required this.birthdate
  });

}