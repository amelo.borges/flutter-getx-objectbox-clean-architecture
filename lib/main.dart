import 'package:estudo_objectbox/routes/app_page.dart';
import 'package:estudo_objectbox/routes/app_routes.dart';
import 'package:estudo_objectbox/presentation/view/users/users_binding.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(const EstudoObjBox());
}

class EstudoObjBox extends StatefulWidget {
  const EstudoObjBox({Key? key}) : super(key: key);

  @override
  State<EstudoObjBox> createState() => _EstudoObjBoxState();
}

class _EstudoObjBoxState extends State<EstudoObjBox> {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialRoute: AppRoutes.users,
      initialBinding: UsersBinding() ,
      getPages: AppPage.items,
    );
  }
}
