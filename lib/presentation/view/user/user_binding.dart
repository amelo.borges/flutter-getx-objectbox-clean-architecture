import 'package:estudo_objectbox/presentation/view/user/user_controller.dart';
import 'package:get/get.dart';

class UserBinding extends Bindings{

  @override
  void dependencies() {
    Get.lazyPut(() => UserController());
  }
}