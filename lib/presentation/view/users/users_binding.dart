import 'package:estudo_objectbox/presentation/view/users/users_controller.dart';
import 'package:get/get.dart';

class UsersBinding extends Bindings{

  @override
  void dependencies() {
    Get.lazyPut(() => UsersController());
  }
}