import 'package:dio/dio.dart';

class HttpManager {

  Future<Map> restRequest({
    required String url,
    required String method,
    Map? headers,
    Map? body,
  }) async {
    final defaultHeaders = headers?.cast<String, String>() ?? {}
      ..addAll({
        'content-type': 'application/json',
        'accept': 'application/json',
      });
    Dio dio = Dio();
    try {

      Response response = await dio.request(
        url,
        options: Options(
          method: method,
          headers: defaultHeaders,
        ),
        data: body,
      );

      return response.data;

    } on DioError catch (e) {
      return e.response?.data ?? {};
    } catch (e) {
      return {};
    }
  }
}
